package com.tw.ca.coversion;

public class ConversionMap {
	/*
	 * A structure to hold each input line after splitting the line on action verb(delimiter).
	 * */
	private int stmtType;			// Statement Type
	private String[] antecedent;    // Part before delimiter
	private String[] consequent;    // Part after delimiter
	public int getStmtType() {
		return stmtType;
	}
	public void setStmtType(int stmtType) {
		this.stmtType = stmtType;
	}
	public String[] getAntecedent() {
		return antecedent;
	}
	public void setAntecedent(String[] strings) {
		this.antecedent = strings;
	}
	public String[] getConsequent() {
		return consequent;
	}
	public void setConsequent(String[] consequent) {
		this.consequent = consequent;
	}
	
}
