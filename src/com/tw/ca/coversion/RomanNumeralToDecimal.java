package com.tw.ca.coversion;

//import java.util.ArrayList;
//import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class RomanNumeralToDecimal {
	/*
	 * A structure to hold the Symbols and values of the basic Roman Numerals
	 * */
	private RomanNumeralToDecimal() { } 
	
	public static final Map<String, Integer> rNDMapping = createMapping();
	
	private static Map<String, Integer> createMapping() {
		Map<String, Integer> mp = new HashMap<String, Integer>();
        mp.put("I", 1);
        mp.put("V", 5);
        mp.put("X", 10);
        mp.put("L", 50);
        mp.put("C", 100);
        mp.put("D", 500);
        mp.put("M", 1000);
        return Collections.unmodifiableMap(mp);
    }
	//public static final ArrayList<Character> allNumerals = new ArrayList<Character>(Arrays.asList('I', 'V', 'X', 'L', 'C', 'D', 'M'));
}
