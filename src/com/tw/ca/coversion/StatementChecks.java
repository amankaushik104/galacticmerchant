package com.tw.ca.coversion;

public final class StatementChecks {
	/*
	 * Various constants used throughout the project.
	 * */
	private StatementChecks() {}
	
	public static final int STMT_TYPE_FUNDAMENTAL = 0;
	public static final int STMT_TYPE_OBSERVATION = 1;
	public static final int STMT_TYPE_QUERY = 2;
	public static final char QUESTION = '?';
	public static final String invalidQuery = "INVALID";
	public static final String invalidQueryOutput = "I have no idea what you are talking about";
	public static final String quntityCheckOne = "MUCH";
	public static final String quntityCheckTwo = "MANY";
	public static final String currencyUnit = "CREDITS";
	public static final String regXCheck = "^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$";
	public static final String invalidNumArgs = "Invalid number of argumets, exiting application.";
	public static final boolean debug = false;
}
