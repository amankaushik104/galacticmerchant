package com.tw.ca.ops;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FileOps {
	/*
	 * This class does all file related tasks(reading and writing)
	 * */
	public ArrayList<String> getFileContent(String fileName) {
		/*
		 * Read contents of the file, identified by the given fileName.
		 * */
		ArrayList<String> content = null;
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
			content = new ArrayList<>();
			try {
				String line;
				while((line = bufferedReader.readLine()) != null){
					content.add(line.trim());
				}
			} catch (IOException e) {
				System.out.println("Error occured during reading file.");
				e.printStackTrace();
			} finally{
				if(bufferedReader != null) {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						System.out.println("Cannot close bufferedReader(TYPE: BufferedReader)");
						e.printStackTrace();
					}
				}
			}
		} 
		catch (FileNotFoundException e) {
			System.out.println("File not found.");
			e.printStackTrace();
		}
		return content;
	}
	public void writeToOutputFile(String content) {
		/*
		 * Write String object to file
		 * */
		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter(new FileWriter("output.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		printWriter.write(content);
	 
		printWriter.close();
	}
}
