package com.tw.ca.source;

import java.net.URL;
import java.util.ArrayList;
import static com.tw.ca.coversion.StatementChecks.*;
import com.tw.ca.ops.FileOps;

public class GalacticMerchantGuide {
	public static void main(String args[]) {
		if(args.length > 1) {
			System.out.println(invalidNumArgs);
		}
		else {
			String inputFileName;
			ArrayList<String> inputFileContent;

			if(args.length == 0)
				inputFileName = getInputResource().getFile();
			else
				inputFileName = args[0];
					
			if(!inputFileName.isEmpty()) {
				FileOps fileOps = new FileOps();
				inputFileContent = fileOps.getFileContent(inputFileName);
				if(debug == true) {
					System.out.println("File content: ");
					for(String line : inputFileContent) {
						System.out.println(line);
					}
				}
				QueryAnswers queryAnswers = new QueryAnswers();
				/*
				 * Get Query Answers for the content of input file
				 * */
				String answer = queryAnswers.getQueryAnswers(inputFileContent, debug);
				fileOps.writeToOutputFile(answer);
				System.out.println("Queries Evaluated, Output written to File(output.txt), created in project root folder.");
			}
			else
				System.out.println("File not found.");
		}
	}
	private static URL getInputResource() {
		URL url = GalacticMerchantGuide.class.getClassLoader().getResource("input/input.txt");
		return url;
	}
}
