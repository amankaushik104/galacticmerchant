package com.tw.ca.source;

import static com.tw.ca.coversion.RomanNumeralToDecimal.rNDMapping;
import static com.tw.ca.coversion.StatementChecks.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.tw.ca.coversion.ConversionMap;
import com.tw.ca.util.InputProcessingUtil;

public class QueryAnswers {
	/*
	 * Process input and return output as String.
	 * */
	
	/*
	 * Maps Fundamental Symbols to Roman Numeral base Symbols
	 * */
	private HashMap<String, String> auxMap;
	
	
	public String getQueryAnswers(ArrayList<String> inputFileContent, boolean debug) {
		ArrayList<ConversionMap> processedContentList = processInput(inputFileContent);

		if (debug == true) {
			System.out.println("Processed content: ");
			for (ConversionMap c : processedContentList) {
				System.out.println(c.getStmtType() + " : " + Arrays.deepToString(c.getAntecedent()) + " : "
						+ Arrays.deepToString(c.getConsequent()));
			}
		}

		return parseProcessedInput(processedContentList, debug);
	}

	private ArrayList<ConversionMap> processInput(ArrayList<String> inputFileContent) {
		InputProcessingUtil inputProcessingUtil = new InputProcessingUtil();
		ArrayList<ConversionMap> processedContentList = new ArrayList<>();
		processedContentList = inputProcessingUtil.process(inputFileContent);
		return processedContentList;
	}

	private String parseProcessedInput(ArrayList<ConversionMap> processedContentList, boolean debug) {

		HashMap<String, Integer> fundamental = creatFundamentalSymbolMap(processedContentList, debug);
		HashMap<String, Double> observation = solveObservations(processedContentList, fundamental, debug);
		return solveQuery(processedContentList, fundamental, observation, debug);
	}

	/*
	 * Mapping of Fundamental Symbols to Roman Numeral Values.
	 * */
	private HashMap<String, Integer> creatFundamentalSymbolMap(ArrayList<ConversionMap> processedContentList,
			boolean debug) {
		auxMap = new HashMap<>();
		HashMap<String, Integer> fundamental = new HashMap<>();
		for (ConversionMap content : processedContentList) {
			if (content.getStmtType() == STMT_TYPE_FUNDAMENTAL) {
				fundamental.put(content.getAntecedent()[0].toString().toLowerCase(),
						rNDMapping.get(content.getConsequent()[0].toString()));
				auxMap.put(content.getAntecedent()[0].toString().toLowerCase(), content.getConsequent()[0].toString());
			}
		}

		if (debug == true) {
			System.out.println("creatFundamentalSymbolMap");
			Iterator<Entry<String, Integer>> it = fundamental.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				System.out.println(pair.getKey() + " = " + pair.getValue());
			}
		}
		return fundamental;
	}

	/*
	 * Map Special Symbols (Gold, Silver) to their calculated values.
	 * */
	private HashMap<String, Double> solveObservations(ArrayList<ConversionMap> processedContentList,
			HashMap<String, Integer> fundamental, boolean debug) {

		HashMap<String, Double> observation = new HashMap<>();

		for (ConversionMap content : processedContentList) {
			if (content.getStmtType() == STMT_TYPE_OBSERVATION) {
				String[] cons = content.getConsequent();
				String currencyVal = cons[0].toString();
				String currency = cons[1].toString().toUpperCase();
				if (currency.equals(currencyUnit)) {
					ArrayList<String> rN = new ArrayList<>();
					for (String s : content.getAntecedent()) {
						if (fundamental.containsKey(s.toLowerCase()))
							rN.add(s.toLowerCase());
						else {
							observation.put(s.toLowerCase(), (double) (Integer.parseInt(currencyVal)
									/ (double) solveRomanNumeral(rN, fundamental)));
						}
					}
				}
			}
		}

		// fundamental.putAll(observation);

		if (debug == true) {
			System.out.println("solveObservations");
			Iterator<Entry<String, Double>> it = observation.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				System.out.println(pair.getKey() + " = " + pair.getValue());
			}
		}
		return observation;
	}
	/*
	 * Check if the given Roman Numeral is valid or not.
	 * */
	private boolean checkRomanNumeralValidity(ArrayList<String> rN) {
		StringBuilder rgx = new StringBuilder();
		for (String r : rN) {
			rgx.append(auxMap.get(r));
		}
		return rgx.toString().matches(regXCheck);
	}
	/*
	 * Get answer to queries using fundamental,  observation and aux map.
	 * */
	private String solveQuery(ArrayList<ConversionMap> processedContentList, HashMap<String, Integer> fundamental,
			HashMap<String, Double> observation, boolean debug) {
		StringBuilder answer = new StringBuilder();
		for (ConversionMap content : processedContentList) {
			if (content.getStmtType() == STMT_TYPE_QUERY) {
				ArrayList<String> rN = new ArrayList<>();
				String[] antc = content.getAntecedent();
				String[] cons = content.getConsequent();
				if (antc[0].equals(invalidQuery) && cons[0].equals(invalidQuery))
					answer.append(invalidQueryOutput);

				else if (Arrays.deepToString(antc).contains(quntityCheckOne.toLowerCase())
						|| Arrays.deepToString(antc).contains(quntityCheckTwo.toLowerCase())) {
					for (int i = 0; i < cons.length - 1; i++) {
						if (fundamental.containsKey(cons[i].toLowerCase()) == true) {
							rN.add(cons[i].toLowerCase());
						} else if (observation.containsKey(cons[i].toLowerCase()) == true) {
							if (checkRomanNumeralValidity(rN) == true) {
								double val = solveRomanNumeral(rN, fundamental)
										* observation.get(cons[i].toLowerCase());
								for (String s : rN)
									answer.append(s + " ");
								answer.append(cons[i].toLowerCase() + " is " + String.valueOf((int) val) + " "
										+ currencyUnit);
								rN.clear();
							} else {
								answer.append(invalidQueryOutput);
							}
							answer.append("\n");
						}
						else {
							rN.clear();
							answer.append(invalidQueryOutput);
							answer.append("\n");
							break;
						}
					}
					if (!rN.isEmpty() && (rN.size() == cons.length - 1)) {
							if (checkRomanNumeralValidity(rN) == true) {
								int val = solveRomanNumeral(rN, fundamental);
								for (String s : rN)
									answer.append(s + " ");
								answer.append("is " + String.valueOf((int) val));
							} else {
								answer.append(invalidQueryOutput);
							}
						answer.append("\n");
					}
				}
			}
		}
		if (debug == true)
			System.out.println(answer.toString());
		return answer.toString();
	}
	
	/*
	 * Return Decimal value of Roman Numeral
	 * */
	private int solveRomanNumeral(ArrayList<String> rN, HashMap<String, Integer> fundamental) {
		int decVal = 0;
		int prevVal = 0;
		int len = rN.size();
		for (int i = len - 1; i >= 0; i--) {
			int aux = fundamental.get(rN.get(i));
			if (aux < prevVal)
				decVal -= aux;
			else
				decVal += aux;

			prevVal = aux;
		}
		return decVal;
	}

}
