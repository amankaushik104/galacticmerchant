package com.tw.ca.source;

import static org.junit.Assert.*;
import static com.tw.ca.coversion.StatementChecks.*;
import java.util.ArrayList;

import org.junit.Test;

public class QueryAnswersTest {

	@Test
	public final void testGetQueryAnswers() {
		
		QueryAnswers queryAnswers = new QueryAnswers();
		ArrayList<String> list = new ArrayList<String>() {
			private static final long serialVersionUID = 1L;
		{
		    add("glob is I");
		    add("pork is V");
		    add("how many Credits is glob prok Silver ?");
		}};
		boolean debug = false;
		assertEquals(invalidQueryOutput, queryAnswers.getQueryAnswers(list, debug).trim().replaceAll("\n", ""));	
	}
}
