package com.tw.ca.util;

import java.util.ArrayList;

import static com.tw.ca.coversion.RomanNumeralToDecimal.*;
import com.tw.ca.coversion.ConversionMap;
import static com.tw.ca.coversion.StatementChecks.*;

public class InputProcessingUtil {
	/*
	 * Convert each input line into a ConversionMap object.
	 * */
	private ArrayList<ConversionMap> list = new ArrayList<>();
	
	public ArrayList<ConversionMap> process(ArrayList<String> content){	
		/*
		 * Split each line base on the type of statement it is (Fundamental, Observation or Query).
		 * */
		for(String line: content) {
			char lastChar = line.charAt(line.length() - 1);
			if(lastChar == QUESTION) {
				splitStatement(line, STMT_TYPE_QUERY);
			}
			else if(rNDMapping.containsKey(String.valueOf(lastChar))) {
				splitStatement(line, STMT_TYPE_FUNDAMENTAL);
			}
			else{
				splitStatement(line, STMT_TYPE_OBSERVATION);
			}
		}
		return list;
	}
	
	private void splitStatement(String line, int isWhichTypeStmt) {
		/*
		 * Fill ConversionMap object for each line after splitting.
		 * */
		ConversionMap conversionMap = new ConversionMap();
		String[] aux = line.split(" is ");
		switch (isWhichTypeStmt) {
		case STMT_TYPE_FUNDAMENTAL:	
			conversionMap.setStmtType(STMT_TYPE_FUNDAMENTAL);
			break;
		case STMT_TYPE_OBSERVATION:
			conversionMap.setStmtType(STMT_TYPE_OBSERVATION);
			break;
		case STMT_TYPE_QUERY:
			conversionMap.setStmtType(STMT_TYPE_QUERY);
			/* last char is '?' */
			break;
		}
		if(aux.length == 1 && isWhichTypeStmt == STMT_TYPE_QUERY) {
			conversionMap.setAntecedent(new String[] {invalidQuery});
			conversionMap.setConsequent(new String[] {invalidQuery});
		}
		else {
			conversionMap.setAntecedent(aux[0].trim().split(" "));
			conversionMap.setConsequent(aux[1].trim().split(" "));
		}
		list.add(conversionMap);
	}
}
